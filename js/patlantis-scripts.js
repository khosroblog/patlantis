$(document).ready(function(){
	$(".scroll-top > a").click(function(){
		$('body,html').animate({
			scrollTop: 0
		}, 800);
		
		return false;
	});
	
	// Fit Images
	$('[class*="wp-image-"]').each(function(){
		var img = $(this), alignClass,
			imgWidth = img.attr("width") || img.width(),
			imgHeight = img.attr("height") || img.height(),
			align = img.attr("class").match(/(align)[\S]+/gi) || "",
			p = $(this).parent(), tag = (p[0].nodeName == "A")? p.parent() : p; 
		
		imgWidth && tag.css({maxWidth: imgWidth + "px", maxheight: imgHeight + "px"});
		if( tag.hasClass("wp-caption") || tag.hasClass("thumbnail-wraper") ){ return ;}
		
		tag.addClass( align + " patlan-caption wp-caption " );
		
	});
	
	// Fit Video's
	$(".article-content").fitVids();
	
	// Comment Form label animate
	var input = $(".comment-form input:not(:submit), .comment-form textarea"),
		is_rtl = $("body").hasClass("rtl");
	
	input.focus(function(){
		var label = $(this).parent().find("label"),
			dir = is_rtl ? {right: '-200px', left: "inherit"} : {left: '-200px', right: "inherit"};
		label.animate(dir) ;
	});
	input.blur(function(){
		var label = $(this).parent().find("label"),
			dir = is_rtl ? {right: 0} : {left: 0};
		label.animate( dir ) ;
	});
	
	// Comment Submit Button Style
	$(".form-submit input").addClass("btn btn-large btn-search");
	
	// Tooltip Slider 	
	$('[id*=tooltip-]').tooltip({
		html: true
	});
	
	// Bootstrap Slider
	$('[id*=patlan-carousel-]').carousel();
	$("a.pause").click(function(){
		var iconPlay = $(this).data("iconplay"),
			iconPause = $(this).data("iconpause"),
			isPlay = $(this).hasClass( iconPlay ),
			parentID  = $(this).attr("href").replace("#patlan-carousel-", "");

		if( isPlay ){
			$(this).removeClass( iconPlay ).addClass( iconPause );
			$("#patlan-carousel-" + parentID ).carousel('cycle');
		}else{
			$(this).removeClass( iconPause ).addClass( iconPlay );
			$("#patlan-carousel-" + parentID ).carousel('pause');
		}
		return false;
	});
	
	 
});