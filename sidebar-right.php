<?php if( apply_filters("patlan_custom_right_sidebar", false ) ): ?>
	<?php do_action("patlan_register_right_sidebar"); ?>
<?php else: ?>
	<?php if(  is_active_sidebar("sidebar-right") ) { ?>
		<?php 
			global $_wp_theme_features;
			
			$sidebar_right_class = "span5";
			if( in_array( 'sidebar-right-class', $_wp_theme_features ) ){
				$sidebar_right_class = $_wp_theme_features['sidebar-right-class'];
				$sidebar_right_class = $sidebar_right_class[0]["class"];
			}
			$sidebar_right_width = esc_attr( $sidebar_right_class );
		?>
		<aside class="sidebar sidebar-right <?php echo $sidebar_right_class; ?> visible-desktop visible-tablet" >  
			<ul class="sidebar-widgets">	
				<?php if( function_exists('dynamic_sidebar') ){ dynamic_sidebar('sidebar-right'); } ?>
			</ul>
		</aside>
	<?php } ?>
<?php endif; ?>