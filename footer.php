		<!-- Footer -->
		<footer class="footer row-fluid">
			<div class="scroll-top" >
				<a href="#" ><i class="icon-scroll-top" ></i></a>
			</div>
			
			<!-- Footer Wraper -->
			<div class="footer-wraper " >
				<ul class="footer-widgets ">	
					<?php if( function_exists('dynamic_sidebar') ){ dynamic_sidebar('footer-widgets'); } ?>
				</ul>
			</div>
			<!-- /Footer Wraper -->
			
			<!-- copyright -->
			<?php 
				$copyright = '<p class="copyright">';
				$copyright .= 'Copyright (C) 2013 | Powered By <a href="http://wordpress.org" >Wordpress</a> | <a href="http://khosroblog.com" > Hadi Khosrojerdi.</a>';
				$copyright .= '</p>';
				
				echo apply_filters( "patlan_footer_copyright", $copyright );
			?>
			<!-- /copyright -->
			
		</footer>
		<!-- /Footer -->
	
	</div>
	<!-- /Wrapper -->
	
	<?php wp_footer();?>
	
	</body>
	
</html>