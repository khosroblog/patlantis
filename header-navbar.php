<nav class="patlan-navbar navbar navbar-inverse" >
	<div class="container">
		<div class="navbar-inner">
		
			<a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			
			<div class='nav-collapse collapse navbar-responsive-collapse' >
			
				<?php 
					$defaults = array(
						'theme_location'  => 'header-menu',
						'fallback_cb'     => 'wp_page_menu',
						'items_wrap'      => '<ul id="%1$s" class="nav %2$s" >%3$s</ul>',
						'depth'           => 0,
					);
					
					wp_nav_menu( $defaults );
				?>
				
			</div><!-- /nav-collapse -->
		</div><!-- /navbar-inner -->
	</div><!-- /container -->
</nav><!-- /navbar -->
