<?php get_header(); ?>
	
	<!-- Main -->
	<div id="main-container" class="main row-fluid " >
	
		<!-- section -->
		<section class="content-section " >
			<article id="post-0" class="post no-results not-found">
				<div class="post-wrap">
					<div class="post-content">
						<h3 class="post-title">
						<?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'patlantis' ); ?>
						</h3>
						<div class="article-content">
						<?php get_search_form(); ?>
						</div>
					</div>
				</div>
			</article>
		</section>
		<!-- /section -->
		
	</div>
	<!-- /Main -->
	
<?php get_footer(); ?>