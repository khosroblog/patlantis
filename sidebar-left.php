<?php if( apply_filters("patlan_custom_left_sidebar", false ) ): ?>
	<?php do_action("patlan_register_left_sidebar"); ?>
<?php else: ?>
	<?php if(  is_active_sidebar("sidebar-left") ) { ?>
		<?php 
			global $_wp_theme_features;
			
			$sidebar_left_class = "span5";
			if( in_array( 'sidebar-left-class', $_wp_theme_features ) ){
				$sidebar_left_class = $_wp_theme_features['sidebar-left-class'];
				$sidebar_left_class = $sidebar_left_class[0]["class"];
			}
			$sidebar_left_class = esc_attr( $sidebar_left_class );
		?>
		<aside class="sidebar sidebar-left <?php echo $sidebar_left_class; ?> visible-desktop visible-tablet" >  
			<ul class="sidebar-widgets">	
				<?php if( function_exists('dynamic_sidebar') ){ dynamic_sidebar('sidebar-left'); } ?>
			</ul>
		</aside>
	<?php } ?>
<?php endif; ?>