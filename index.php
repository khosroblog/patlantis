<?php get_header(); global $post, $patlan_icons , $wp_query; ?>
	
	<!-- Main -->
	<div id="main-container" class="main row-fluid " >
		
		<!-- Left Sidebar -->
		<?php $sidebar_left = ( is_rtl() )? "right" : "left" ; ?>
		<?php get_sidebar("{$sidebar_left}"); ?>
		<!-- /Left Sidebar -->
		
		<!-- Main Section -->
		<?php 
			$content_section_class = patlan_content_section_class();
			$content_section_class = esc_attr( $content_section_class );
		?>
		
		<section class="main-section <?php echo $content_section_class;?>" >
		<?php do_action("patlan_prepend_content_section"); ?>
			
			<!-- Content Section -->
			<section class="content-section" >
			
			<?php global $query_string;
			$args = array(
					"post_type"		=>	"post",
					"post_status"	=>	"publish",
					"post_per_page"	=>	get_option("posts_per_page"),
					);
			$args = wp_parse_args( $query_string, $args );
			$args = apply_filters("patlan_register_query_args", $args);
			query_posts( $args ); ?>
			<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
			
				<?php $format = ( get_post_format() )? get_post_format() : "standard" ; ?>
				
				<!-- Post-<?php echo $format . "-" . get_the_ID(); ?> -->
				<?php  get_template_part( 'formats/format', $format ); ?>
				<!-- /Post-<?php echo $format . "-" . get_the_ID(); ?> -->
				
			<?php endwhile; ?>
			<?php else: ?>
			<article id="post-0" class="post no-results not-found">
				<div class="post-wrap">
					<div class="post-content">
						<h3 class="post-title">
						<?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'patlantis' ); ?>
						</h3>
						<div class="article-content">
						<?php get_search_form(); ?>
						</div>
					</div>
				</div>
			</article>
			<?php endif; // end post loop ?>
			
			<!-- Posts Pagination -->
			<div class="posts-paginate" >
			<?php echo patlan_posts_paginate(); ?>
			</div>
			<!-- /Posts Pagination -->
		
			<?php wp_reset_query(); ?>
			
			</section>
			<!-- /Content Section -->
			
		</section>
		<!-- /Main Section -->
		
		<!-- Right Sidebar -->
		<?php $sidebar_right = ( is_rtl() )? "left" : "right" ; ?>
		<?php get_sidebar("{$sidebar_right}"); ?>
		<!-- /Right Sidebar -->
		
	</div>
	<!-- /Main -->
	
<?php get_footer(); ?>