<?php 
	
	/* After Theme Setup */
	/*-----------------------------------------------------------------------------------*/
	add_action('after_setup_theme', 'after_setup_patlantis');
	function after_setup_patlantis(){
		
		/* Constants */
		defined("PATLAN_DIR_THEME")		? NULL : define("PATLAN_DIR_THEME", trailingslashit( get_template_directory_uri() ) );
		defined("PATLAN_DIR_IMAGES")	? NULL : define("PATLAN_DIR_IMAGES", PATLAN_DIR_THEME . trailingslashit( "images" ) );
		defined("PATLAN_DIR_JS")		? NULL : define("PATLAN_DIR_JS", PATLAN_DIR_THEME . trailingslashit( "js" ) );
		defined("PATLAN_DIR_CSS")		? NULL : define("PATLAN_DIR_CSS", PATLAN_DIR_THEME . trailingslashit( "css" ) );
		defined("PATLAN_DIR_FONTS")		? NULL : define("PATLAN_DIR_FONTS", PATLAN_DIR_CSS . "fonts" );
		defined("PATLAN_THEME_VERSION")	? NULL : define("PATLAN_THEME_VERSION", "1.0.0" );
		
		load_theme_textdomain('patlantis', get_template_directory() . '/languages');
	
		add_theme_support('html5', array( 'comment-form', 'comment-list' ) );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support('post-formats', array('audio','status', 'quote', 'video', 'gallery', 'link', 'image') );
		add_theme_support('post-thumbnails', array('post') );
		add_image_size( 'thumb_768x350', 768, 350, true );
		add_image_size( 'thumb_50x50', 50, 50, true );
		add_editor_style("css/editorstyle.css");
		add_theme_support( 'content-width', 1024 );
		
		// Sidebar Section Bootstrap Class
		add_theme_support( 'sidebar-left-class', array("class" => "span5") );
		add_theme_support( 'sidebar-right-class', array("class" => "span5") );
		
		// Content Section Bootstrap Class
		add_theme_support( 'content-section-class', array(
			"one_sidebar"		=>	"span11",
			"two_sidebar"		=>	"span6",
			"without_sidebar"	=>	"span16",
		) );
		
		register_nav_menu( 'header-menu', __('Header Menu', 'patlantis') );
		
		// Disable default gallery style
		add_action( "use_default_gallery_style", "__return_false" );
	}
	
	/* Thumbnail */
	/*-----------------------------------------------------------------------------------*/
	
	function patlan_post_thumbnail( $size="thumb_768x350" ){
		global $post;
		
		$output = "";
		if( has_post_thumbnail() || wp_attachment_is_image() ) : 
			$thumb_id = ( $id = get_post_thumbnail_id( $post->ID ) )? (int) $id : (int) $post->ID ;
			$thumb_768x350 =  wp_get_attachment_image_src( $thumb_id, $size );
			$thumb_fullsize =  wp_get_attachment_image_src( $thumb_id, "large" );
			
			$output = '<div class="post-thumbnail" >';
			$output .= '<div class="thumbnail-wraper" >';
			$output .= '<img  data-fullsize="%1$s" width="%2$d" height="%3$d" class="wp-image-%4$d" src="%5$s" alt="%6$s" title="%7$s" >';
			$output .= '</div>';
			$output .= '</div>';
			
			$output = sprintf( $output,
							$thumb_fullsize[0],
							(int) $thumb_768x350[1],
							(int) $thumb_768x350[2],
							(int) $thumb_id ,
							esc_attr( $thumb_768x350[0] ),
							esc_attr( get_the_excerpt() ),
							esc_attr( get_the_title() ) 
					); 
		endif ; 
		
		return apply_filters( "patlan_register_post_thumbnail", $output );
	}
	
	/* Register Scripts & Styles */
	/*-----------------------------------------------------------------------------------*/
	
	function patlan_enqueue_styles(){
		if( is_admin() ){ return; }
		wp_enqueue_style("patlan-bootstrap", PATLAN_DIR_CSS . "bootstrap.min.css", array(), "2.3.2" , "screen");
		wp_enqueue_style("patlan-font-awesome",  PATLAN_DIR_CSS . "font-awesome.css", array(), "3.2.1" , "screen" );
		wp_enqueue_style("patlan-default", PATLAN_DIR_CSS . "default.css", array(), PATLAN_THEME_VERSION . rand(1, 100) , "screen" );
		
		// Responsive Styles
		wp_enqueue_style("patlan-desktop", PATLAN_DIR_CSS . "desktop.css", array(), PATLAN_THEME_VERSION . rand(1, 100), "screen and (min-width: 980px )" );
		wp_enqueue_style("patlan-tablet-landscape", PATLAN_DIR_CSS . "tablet.css", array(), PATLAN_THEME_VERSION . rand(1, 100), "screen and ( max-width: 979px ) and ( min-width: 768px )" );
		wp_enqueue_style("patlan-tablet", get_bloginfo("stylesheet_url"), array(), PATLAN_THEME_VERSION . rand(1, 100), "screen" );
		wp_enqueue_style("patlan-phone", PATLAN_DIR_CSS . "phone.css", array(), PATLAN_THEME_VERSION . rand(1, 100), "screen and (max-width: 480px)" );
		
		// Gallery Style
		if( is_singular() ){
			wp_enqueue_style("patlan-gallery", PATLAN_DIR_CSS . "gallery-style.css" , array(), PATLAN_THEME_VERSION , "screen" );
		}
	}
	add_action("wp_enqueue_scripts", "patlan_enqueue_styles", 1 );
	
	function patlan_enqueue_scripts(){
		if( is_admin() ){ return; }
		wp_deregister_script("jquery");
		wp_enqueue_script("patlan-jquery", PATLAN_DIR_JS . "jquery-1.10.2.js", array(), "1.10.2");
	
		wp_enqueue_script("patlan-bootstrap", PATLAN_DIR_JS . "bootstrap.min.js", array(), "2.3.2", true );
		wp_enqueue_script("patlan-theme-scripts", PATLAN_DIR_JS . "patlantis-scripts.js", array(), PATLAN_THEME_VERSION , true );
		wp_enqueue_script("patlan-fitvids", PATLAN_DIR_JS . "jquery.fitvids.js", array(), "1.0", true );
		
		if ( is_singular() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
		
	}
	add_action("wp_enqueue_scripts", "patlan_enqueue_scripts", 2, 0);
	
	function patlan_print_font_styles(){
		$output = '
		<style>
			@font-face{
				font-family:"Yekan";
				src:url( "%1$s/BYekan/BYekan.eot");
				src:url( "%1$s/BYekan/BYekan.eot#iefix") format("eot"),
					url( "%1$s/BYekan/BYekan.woff") format("woff"),
					url( "%1$s/BYekan/BYekan.ttf") format("truetype");
				font-size: 30px;
				font-widget: normal;
			}
			
			@font-face{
				font-family:"PrintClearly";
				src:url( "%1$s/print/PrintClearly_TT.eot");
				src:url( "%1$s/print/PrintClearly_TT.eot#iefix") format("eot"),
					url( "%1$s/print/PrintClearly_TT.woff") format("woff"),
					url( "%1$s/print/PrintClearly_TT.ttf") format("truetype");
				font-size: 30px;
				font-widget: normal;
			}
			
		</style>';
		
		$output = sprintf( $output, PATLAN_DIR_FONTS );
		
		echo apply_filters("patlan_register_custom_font", $output);
	}
	add_action("wp_head", "patlan_print_font_styles", 45, 0);
	
	function patlan_post_content_wordwrap(){
		$output = '<style>
		article.hentry .post-content {
			word-wrap: break-word !important;
			word-break: break-word; 
			-ms-word-wrap: break-word !important;
		}
		</style>';
		
		echo $output;
	}
	add_action("wp_head", "patlan_post_content_wordwrap", 46);
	
	function patlan_ie_scripts(){
		?>
		<!--[if lt IE 9]>
		<script src="<?php echo PATLAN_DIR_JS; ?>html5.js" ></script>
		<script src="<?php echo PATLAN_DIR_JS; ?>respond.min.js" ></script>
		<![endif]-->
		<?php 
		
	}
	add_action("wp_head", "patlan_ie_scripts", 50, 0);
	
	function patlan_content_width(){
		global $_wp_theme_features;
		
		if( !in_array( 'content-width', $_wp_theme_features ) ){ $_wp_theme_features[ 'content-width' ] = array(1024); }
		$content_width = intval( $_wp_theme_features[ 'content-width' ][0] );
		$output = '<style>.wrapper{max-width: %spx;}</style>';
		
		echo sprintf( $output , $content_width );
	}
	add_action("wp_head", "patlan_content_width", 51, 0);
	
	/* Default Navigation Menu */
	/*-----------------------------------------------------------------------------------*/
	function patlan_header_nav_menu( $menu ){
	
		$menu = str_replace('class="nav"', 'class="navbar-inner"', $menu);
		$menu = str_replace("<ul>", "<ul class='nav'>", $menu);
		
		return $menu;
	}
	add_action("wp_page_menu", "patlan_header_nav_menu", 3);
	
	/* Register Sidebar */
	/*-----------------------------------------------------------------------------------*/
	
	function patlan_register_sidebars(){
		// Left Sidebar Widgets
		$args = array(
			'name'          => __( 'Sidebar Left', 'patlantis' ),
			'id'            => "sidebar-left",
			'description'   => __( 'Widgets in this area will be shown on the left side.', 'patlantis' ),
			'before_widget' => '<li id="%1$s" class="widget %2$s">',
			'after_widget'  => "</li>",
			'before_title'  => '<h6 class="widgettitle"><span><i class="%1$s %2$s" ></i> ',
			'after_title'   => "</span></h6>",
		);
		register_sidebar( $args );
		
		// Right Sidebar Widgets
		$args = array(
			'name'          => __( 'Sidebar Right', 'patlantis' ),
			'id'            => "sidebar-right",
			'description'   => __( 'Widgets in this area will be shown on the right side.', 'patlantis' ),
			'before_widget' => '<li id="%1$s" class="widget %2$s">',
			'after_widget'  => "</li>",
			'before_title'  => '<h6 class="widgettitle"><span><i class="%1$s %2$s" ></i> ',
			'after_title'   => "</span></h6>",
		);
		register_sidebar( $args );

		// Header Widgets
		$args = array(
			'name'          => __( 'Header Widgets', 'patlantis' ),
			'id'            => "header-widgets",
			'description'   => __( 'Widgets in this area will be shown on the header.', 'patlantis' ),
			'before_widget' => '<li id="%1$s" class="widget %2$s">',
			'after_widget'  => "</li>",
			'before_title'  => '<h6 class="widgettitle">',
			'after_title'   => "</h6>",
		);
		register_sidebar( $args );
		
		// Footer Widgets
		$args = array(
			'name'          => __( 'Footer Widgets', 'patlantis' ),
			'id'            => "footer-widgets",
			'description'   => __( 'Widgets in this area will be shown on the footer.', 'patlantis' ),
			'before_widget' => '<li id="%1$s" class="widget %2$s">',
			'after_widget'  => "</li>",
			'before_title'  => '<h6 class="widgettitle">',
			'after_title'   => "</h6>",
		);
		register_sidebar( $args );
	}
	add_action("init", "patlan_register_sidebars");
	
	function patlan_icon_widgettitle( $sidebars ){
		global $wp_registered_widgets, $patlan_icons;
		
		$id = $sidebars[0]['widget_id'];
		$classname_ = '';
		foreach ( (array) $wp_registered_widgets[$id]['classname'] as $cn ) {
			if ( is_string( $cn ) ){
				$classname_ .= '_' . $cn;
			}elseif( is_object( $cn ) ){
				$classname_ .= '_' . get_class($cn);
			}
		}
		$classname_ = ltrim($classname_, '_');
		if( $sidebars[0]["id"] == "sidebar-right" || $sidebars[0]["id"] == "sidebar-left" ){
			if( in_array( $classname_ , array_keys( $patlan_icons["sidebar_widgets"] ) ) ){
				$sidebars[0]["before_title"] = sprintf( $sidebars[0]["before_title"] , $patlan_icons["sidebar_widgets"][ $classname_ ], "icon-{$classname_}" );
			}
		}
		
		apply_filters("patlan_register_icon_widgettitle", $sidebars , $classname_ );
		
		return $sidebars;
	}
	add_action("dynamic_sidebar_params", "patlan_icon_widgettitle");
	
	function patlan_content_section_class(){
		global $_wp_theme_features;
		
		if( in_array( 'content-section-class', $_wp_theme_features ) ){
			$cs_class = $_wp_theme_features['content-section-class'];
			$one_sidebar = $cs_class[0]['one_sidebar'];
			$two_sidebar = $cs_class[0]['two_sidebar'];
			$without_sidebar = $cs_class[0]['without_sidebar'];
		}
		
		// Without Sidebar
		$class = $without_sidebar;
		
		// One Sidebar
		if( is_active_sidebar("sidebar-left") xor is_active_sidebar("sidebar-right") ){
			$class = $one_sidebar;
		
		// Two Sidebar
		}elseif( is_active_sidebar("sidebar-left") && is_active_sidebar("sidebar-right") ){
			$class = $two_sidebar;
		}
		
		return $class;
	}
	
	/* Register Global Icons */
	/*-----------------------------------------------------------------------------------*/
	
	function patlan_global_icons(){
		global $patlan_icons;
		
		// Global Post icons
		$patlan_icons["post-icons"] = array( 
			"icon-comments"			=>	"icon-comments",
			"icon-edit-post-link"	=>	"icon-edit, icon-white",
			"icon-sticky-post"		=>	"icon-paperclip",
			"icon-post-category"	=>	"icon-folder-open",
			"icon-post-format"		=>	array(
											"format-standard"	=>	"icon-pencil",
											"format-status"		=>	"icon-globe",
											"format-image"		=>	"icon-camera-retro",
											"format-gallery"	=>	"icon-picture",
											"format-quote"		=>	"icon-quote-left",
											"format-link"		=>	"icon-link",
											"format-video"		=>	"icon-film",
											"format-audio"		=>	"icon-microphone",
											"format-chat"		=>	"icon-comments-alt"
										)
		);
		
		// Sidebar Widget Icons
		$patlan_icons["sidebar_widgets"]	= array(
				"widget_recent_entries"		=>	"icon-pencil",
				"widget_meta"				=>	"icon-user",
				"widget_tag_cloud"			=>	"icon-tags",
				"widget_categories"			=>	"icon-folder-open",
				"widget_nav_menu"			=>	"icon-th-list",
				"widget_text"				=>	"icon-file-text",
				"widget_search"				=>	"icon-search",
				"widget_calendar"			=>	"icon-calendar",
				"widget_archive"			=>	"icon-archive",
				"widget_rss"				=>	"icon-rss",
				"widget_recent_comments"	=>	"icon-comments",
				"widget_pages"				=>	"icon-file"
		);
		
		// Miscellaneous
		$patlan_icons["misc"] = array(
			"icon-search-button"	=>	"icon-search",
			"icon-slider-play"		=>	"icon-play",
			"icon-slider-pause"		=>	"icon-pause",
			"icon-slider-prev"		=>	"icon-chevron-left",
			"icon-slider-next"		=>	"icon-chevron-right",
		); 
		$patlan_icons = apply_filters("patlan_register_global_icons", $patlan_icons );
		$patlan_icons = array_map("str_replace_icons", $patlan_icons );
		
	}
	add_action("init", "patlan_global_icons");
	
	function str_replace_icons( $i ){
		if( is_array( $i ) ){
			return array_map("str_replace_icons", $i);
		}else{
			return esc_attr( str_replace(",", " ", $i ) );
		} 
	}
	
	/* Post Functions */
	/*-----------------------------------------------------------------------------------*/
	function patlan_post_excerpt(){
		add_filter( 'excerpt_length', function(){ return 100; }, 999 );
		return apply_filters('the_excerpt', get_the_excerpt());
	}
	
	if( apply_filters("patlan_remove_wpautop_filter", true) ){
		remove_filter ('the_content', 'wpautop');
	}
	
	function patlan_post_content(){
		global $post;
		
		$is_gallery_post = has_post_format("gallery") && !is_singular();
		if( $is_gallery_post ){
			$content = patlan_post_excerpt();
			
		}else{
			$content = apply_filters( 'the_content', get_the_content() );
			if( wp_attachment_is_image() ){
				remove_filter ('the_content', 'prepend_attachment');
				$content = ( $c = apply_filters( 'the_content', get_the_content() ) )? $c : patlan_post_excerpt();
			}
		}
		$content = str_replace( ']]>', ']]&gt;', $content );
		
		if( apply_filters("patlan_remove_wpautop_filter", true) ){
			$content = wpautop( $content , false );
		}else{
			$content = wpautop( $content , true );
		}
		
		return apply_filters("patlan_register_post_content", $content);
	}
	
	function patlan_post_entrymeta(){
	
		if( apply_filters("patlan_custom_entrymeta_post", false) ):
			do_action("patlan_register_entrymeta_post");
		else: 
			get_template_part("formats/post", "entrymeta");
		endif;
	}
	
	function patlan_sticky_post_icon(){
		global $patlan_icons;
		
		if( is_sticky() ){ // Sticky Posts ?>
			<?php $icon_sticky_post = $patlan_icons["post-icons"]["icon-sticky-post"];?> 
			<i class="<?php echo esc_attr( $icon_sticky_post );?>"></i>
		<?php }
	}
	
	function patlan_single_post_entrymeta(){
		global $post;
		$output = <<<OTP
		<div class="entry-meta" >
			<ul>	
				<li class="the-author" >%1\$s</li>
				<li class="the-date" >%2\$s</li>
				<li class="the-category" >%3\$s</li>
			</ul>
		</div> 
OTP;
		$output = sprintf( $output, 
				sprintf( __("<span>By:</span> %s", "patlantis"), get_the_author() ),
				sprintf( __("<span>Date:</span> %s", "patlantis"), get_the_date() ),
				sprintf( __("<span>Category:</span> %s", "patlantis"), get_the_category_list( __(", ","patlantis") ) )
		);
		
		return apply_filters("patlan_register_single_post_entrymeta", $output );
	}
	
	function patlan_post_categories(){
		global $patlan_icons, $post;
		$output = <<<OTP
		<div class="post-categories" >
			<i class="%1\$s" ></i>
			%2\$s
		</div>
OTP;
		$output = sprintf( $output, 
					$patlan_icons['post-icons']['icon-post-category'],
					get_the_category_list( __(" / ","patlantis") )
				);
			
		return apply_filters("patlan_append_post_content", $output);
	}
	
	/* Pagination */
	/*-----------------------------------------------------------------------------------*/
	
	function patlan_posts_paginate(){
		global $wp_query;
		
		$big = 999999999; // need an unlikely integer
		$pagination =  "<div class='pagination'>";
		$attr = array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'type'	 =>	'list',
			'end_size'     => 3,
			'mid_size'     => 0,
			'prev_text'    => is_rtl()? str_replace( "&raquo;", "&laquo;", __('&laquo; Previous', 'patlantis') ) : __('&laquo; Previous', 'patlantis') ,
			'next_text'    => is_rtl()? str_replace( "&laquo;", "&raquo;", __('Next &raquo;', 'patlantis') ) : __('Next &raquo;', 'patlantis') ,
			'current' => max( 1, get_query_var('paged') ),
			'total' => (int) $wp_query->max_num_pages
		);
		$pagination .= paginate_links( $attr );
		$pagination .= "</div>"; 
		
		return apply_filters( "patlan_register_posts_paginate", $pagination, $attr );	
	}
	
	function patlan_pages_paginate(){
		$wp_link_pages = wp_link_pages(array(
				'before'   	=> '<div class="pagination pagination-small wp-link-paginate" ><h5>' . __('Pages:', 'patlantis') . '</h5><ul>',
				'after'    	=> '</ul></div>',
				'echo'		=> false
		)); 
		
		return apply_filters("patlan_register_pages_paginate", $wp_link_pages ); 
	}
	
	function patlan_comments_paginate(){
		$before = '<nav class="pagination pagination-small comment-pagination" role="navigation">';
		$attr = array(
			'type'	=> 'list',
			'echo'	=> false	
		);
		$after = '</nav>';
		$p_comments = $before . paginate_comments_links( $attr ) . $after ;
		
		return apply_filters("patlan_register_comments_paginate", $p_comments);
	}
	
	/* Miscellaneous */
	/*-----------------------------------------------------------------------------------*/
	if( !function_exists("has_shortcode") ):
		function has_shortcode( $shortcode = NULL, $post ) {
			
			$found = false;
			if ( ! $shortcode ) { return $found; }
			if ( stripos( $post->post_content, '[' . $shortcode) !== FALSE ) {
				$found = TRUE;
			}
			return $found;
		}
	endif;
	
	function patlan_link_pages_style( $link  ){
		return ( (int) $link )? "<li><span>" . $link . "</span></li>" : "<li>" . $link . "</li>" ;
	}
	add_action("wp_link_pages_link", "patlan_link_pages_style");
	
	
	function patlan_search_form( $form ) {
		global $patlan_icons;
		
		$form = '<form class="" role="search" method="get" id="searchform" action="%1$s" >
		<div class="table-row" >
			<div class="table-cell btn-column" >
				<button type="submit" class="btn btn-medium btn-search"  value="" >
					<i class="%2$s" ></i>
				</button>
			</div>
			<div class="table-cell text-column" >
				<input type="text" value="%3$s" name="s" id="s" placeholder="%4$s" >
			</div>
		</div>
		</form>';
		
		return sprintf( $form,
						get_home_url( '/' ), $patlan_icons["misc"]["icon-search-button"], get_search_query(), __("Search...", "patlantis") );
	}
	add_filter( 'get_search_form', 'patlan_search_form' );

?>