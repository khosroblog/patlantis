
<!DOCTYPE HTML > 
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?> >
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?> >
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?> >
<!--<![endif]-->

	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" >
		<meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0">
		<title>
			<?php echo esc_html( get_bloginfo("name") ) . wp_title(" | ", false); ?>
		</title>
		<?php global $post;
		
		$keywords = "";
		$desc = get_bloginfo("description");
		if( is_singular()  ){
			$terms = wp_get_post_tags( $post->ID );
			if(  !is_wp_error( $terms ) || !empty( $terms ) ){
				$terms = array_map(function( $t ){
					return $t->name ;
				}, $terms );
				$keywords = implode(", ", $terms);
			}
			
			$desc = $post->post_content ;
			$desc = strip_shortcodes( $desc );
			$desc = str_replace(']]>', ']]&gt;', $desc);
			$desc = wp_trim_words( $desc, 150 );
			$desc = ( $desc )? $desc :  get_the_excerpt() ;
		}
		
		?>
		<?php 
			$meta_tags = array(
							"description"					=>	$desc,
							"keywords"						=>	$keywords,
							"twitter:widgets:link-color"	=>	"#3ABEC0"
						);
			$meta_tags = apply_filters("patlan_register_header_meta_tags", $meta_tags );
		?>
		<?php foreach( (array) $meta_tags as $name => $content ){ ?>
		<meta name="<?php echo esc_attr( $name ); ?>" content="<?php echo esc_attr( $content ); ?>">
		<?php } ?>
		
		<link rel="profile" href="http://gmpg.org/xfn/11" >
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" >
		
		<?php wp_head(); ?>
		
	</head>
	<body <?php body_class();?> >

	<!-- wrapper -->
		<div  class="wrapper">
	
			<!-- header -->
			<header class="header row-fluid">
				
					<!-- logo -->
					<?php 
						$site_logo  = '<div class="logo span7 ">';
						$site_logo .= '<h1 class="site-name" >';
						$site_logo .= '<a href="%1$s">%2$s</a>';
						$site_logo .= '</h1>';
						$site_logo .= '<p class="site-description" >%3$s</p>';
						$site_logo .= '</div>';
						
						$site_logo = sprintf( $site_logo, get_home_url(), get_bloginfo("name"), get_bloginfo('description') );
						
						echo apply_filters("patlan_register_site_logo", $site_logo );
					?>
					<!-- /logo -->
					
					<!-- Widgets -->
					<div class="span9 widgets-wraper" >
						<ul class="header-widgets">	
							<?php if(!function_exists('dynamic_sidebar') || !dynamic_sidebar('header-widgets')); ?>
						</ul>
					</div>
					<!-- /Widgets -->
				
			</header>
			<!-- /header -->
			
			<!-- Header Navigation Bar  -->
			<?php get_template_part("header", "navbar"); ?>
			<!-- /Header Navigation Bar  -->
				
			<?php do_action("patlan_after_menu_navi"); ?>
			