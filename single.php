<?php get_header(); global $post, $patlan_icons ; ?>
	
	<!-- Main -->
	<div id="main-container" class="main row-fluid " >
		
		<!-- Left Sidebar -->
		<?php $sidebar_left = ( is_rtl() )? "right" : "left" ; ?>
		<?php get_sidebar("{$sidebar_left}"); ?>
		<!-- /Left Sidebar -->
		
		<!-- Main Section -->
		<?php 
			$content_section_class = patlan_content_section_class();
			$content_section_class = esc_attr( $content_section_class );
		?>
		<section class="main-section  <?php echo $content_section_class;?>" >
			
			<section class="content-section" >
				<?php do_action("patlan_prepend_content_section"); ?>
			
				<?php global $query_string; ?>
				<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
				
					<?php $format = ( get_post_format() )? get_post_format() : "standard" ; ?>
					
					<article  lang="en" <?php post_class() ;?> >
						
						<!-- Post Thumbnail -->
						<?php echo patlan_post_thumbnail(); ?>
						<!-- /Post Thumbnail -->
						
						<div class="post-wrap" >
						
							<div class="post-content " >
								<h3 class="post-title" ><?php patlan_sticky_post_icon(); ?><?php echo esc_html( get_the_title() ); ?></h3>
								<?php echo patlan_single_post_entrymeta(); ?>
								
								<div class="article-content" >
									<?php echo patlan_post_content(); ?>
								</div>
								
								<!-- Single Post Pagination -->
								<?php  echo patlan_pages_paginate(); ?>
								<!-- /Single Post Pagination -->
							
								<?php do_action("patlan_append_singlepost_content"); ?>
								
							</div><!-- /Post Content -->
							
							<div class="after-singlepost" >
								<?php 
									global $patlan_icons;
									$i = $patlan_icons["sidebar_widgets"]["widget_tag_cloud"];
									$i = sprintf( "<i class='%s' ></i>", $i );
									$before = "<ul class='post-tags' ><li class='title-tags'>{$i}</li><li class='tag' >" ;
									$sep = __(", ","patlantis") . "</li><li class='tag' >";
									$after = "</ul>";
								?>
								<?php $tags = get_the_tag_list( $before, $sep, $after ); ?>
								<?php echo apply_filters( "patlan_after_singlepost", $tags ); ?>
							</div>
							
						</div><!-- /Post Wrap -->
						
					</article>
					
				<?php endwhile; ?>
				<?php else: ?>
				<article id="post-0" class="post no-results not-found">
					<div class="post-wrap">
						<div class="post-content">
							<h3 class="post-title">
							<?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'pasargad' ); ?>
							</h3>
							<div class="article-content">
							<?php get_search_form(); ?>
							</div>
						</div>
					</div>
				</article>
				<?php endif; // end post loop ?>
				
			</section>
			<!-- /Content Section -->
			
			<!-- Comments Section -->
			<section class="comments-section " >
				<div class="comments-wraper" >
					<ul class="commentlist">
						<?php comments_template(); ?>
					</ul>
				</div>
			</section>
			<!-- /Comments Section -->
			
		</section>
		<!-- /Main Section -->
		
		<!-- Right Sidebar -->
		<?php $sidebar_right = ( is_rtl() )? "left" : "right" ; ?>
		<?php get_sidebar("{$sidebar_right}"); ?>
		<!-- /Right Sidebar -->
		
	</div>
	<!-- /Main -->
	
<?php get_footer(); ?>