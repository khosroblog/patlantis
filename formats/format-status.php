<?php global $post, $patlan_icons; ?>
	<article  lang="en" <?php post_class() ;?> >
	
		<div class="post-wrap" >
		
			<!-- Post Entry Meta -->
			<?php patlan_post_entrymeta(); ?>
			<!-- /Post Entry Meta -->
			
			<div class="post-content " >
			
				<div class="post-author" >
					<div class="author-avatar table-cell" >
						<?php $author_id = get_the_author_meta("ID"); ?>
						<?php $author_url = get_the_author_meta("url"); ?>
						<?php 
							if ( $author_url ) {
									echo '<a href="' . esc_url( get_the_author_meta('url') ) . '" title="' . esc_attr( sprintf(__("Visit %s&#8217;s website", "patlantis"), get_the_author() ) ) . '" rel="author external">' . get_avatar( $author_id, 50 ) . '</a>';
							} else {
									echo get_avatar( $author_id, 50 );
							}
						?>
					</div>
					<div class="table-cell" >
						<h5 class="post-title" ><?php echo get_the_author_link() ; ?></h5>
						<div class="article-content" >
							<?php echo patlan_post_content();?>
						</div>
					</div>
				</div>
				
				<?php echo patlan_post_categories() ;?>
				
			</div>
			
		</div>
	</article>