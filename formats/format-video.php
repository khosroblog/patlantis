<article  lang="en" <?php post_class() ;?> >
	<div class="post-wrap" >
		
		<!-- Post Entry Meta -->
		<?php patlan_post_entrymeta(); ?>
		<!-- /Post Entry Meta -->
		
		<div class="post-content " >
			<h3 class="post-title" >
				<?php patlan_sticky_post_icon(); ?>
				<a href="<?php the_permalink();?>" alt="<?php the_title_attribute();?>" >
					<?php echo esc_html( get_the_title() );?>
				</a>
			</h3>
			
			<div class="article-content" >
				<?php echo patlan_post_content();?>
			</div>
			
			<?php echo patlan_post_categories() ;?>
			
		</div>
		
	</div>
</article>