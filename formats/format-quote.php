<?php global $post, $patlan_icons; ?>
	<article  lang="en" <?php post_class() ;?> >
		<div class="post-wrap" >
		
			<!-- Post Entry Meta -->
			<?php patlan_post_entrymeta(); ?>
			<!-- /Post Entry Meta -->
			
			<div class="post-content " >
				<div class="article-content" >
					<?php echo patlan_post_content(); ?>
				</div>
				
				<?php echo patlan_post_categories(); ?>
				
			</div>
			
		</div>
	</article>