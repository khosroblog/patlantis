<?php global $patlan_icons, $post; ?>
<?php  $reset_post = $post ; $parent_id = get_the_ID(); ?>

<div class="post-thumbnail" >
	<div class="thumbnail-wraper" >
			
		<div class="carousel slide" id="patlan-carousel-<?php echo (int) $parent_id;?>">
			
			<?php 
				$has_gallery = has_shortcode( get_the_content( $parent_id ), 'gallery' );
				$thumb_id = get_post_thumbnail_id( $parent_id );
				$args = array(
						'post_parent' 		=> 	$parent_id,
						'post_type'   		=> 	'attachment', 
						'numberposts' 		=> 	-1,
						'post_status' 		=> 	'inherit',
						"post_mime_type"	=>	"image"
				);
				$attachments = get_children( $args ); 
				if( has_post_thumbnail() ){
					$attachments[$thumb_id] = get_post( $thumb_id ); 
				}
			?>
			
			<?php if( $has_gallery ): ?>
			<ol class="carousel-indicators">
				<?php 
					$i = 0;
					foreach( $attachments as $attach_id => $attach ){
						$image = wp_get_attachment_image_src( $attach_id, "thumb_50x50");
						$image_src = esc_attr( $image[0] );
				?>
					<li>
						<a id="tooltip-<?php echo (int) $i;?>" title="<img src='<?php echo  $image_src ;?>' >"  data-slide-to="<?php echo (int) $i;?>" data-target="#patlan-carousel-<?php echo (int) $parent_id;?>" ></a>
					</li>
				<?php $i++; } ?>
			</ol>
			<?php endif; // if( $has_gallery ) ?>
			
			<div class="carousel-inner">
			<?php 
			foreach( $attachments as $post ): setup_postdata( $post ); 
				$image = wp_get_attachment_image_src( get_the_ID(), "thumb_768x350");
				$image_src = esc_attr( $image[0] );
				$img_width = (int) $image[1];
				$img_height = (int) $image[2];
			?>
			  <div id="item-<?php the_ID();?>" class="item">
				<div class="align-none" >
					<img width="<?php echo $img_height;?>" height="<?php echo $img_width;?>" title="<?php the_title_attribute();?>" class="alignnone" alt="<?php the_excerpt();?>" src="<?php echo $image_src; ?>" >
				</div>
				<div class="carousel-caption">
					<h4>
						<?php echo esc_html( get_the_title() );?>
					</h4>
					<p><?php the_excerpt();?></p>
				</div>
				<?php 
					$icon_editpost_link = $patlan_icons["post-icons"]["icon-edit-post-link"];
					edit_post_link( "<i class='{$icon_editpost_link}'></i>"); ?>
			  </div>
			  <?php endforeach; ?>
			  <?php $post = $reset_post; wp_reset_postdata(); ?>
			  
			</div>
			
			<?php if( $has_gallery ): ?>
			<div class="carousel-wrap-controler" >
				<a data-slide="prev"  href="#patlan-carousel-<?php echo (int) $parent_id;?>" class="left  <?php echo $patlan_icons["misc"]["icon-slider-prev"]; ?> carousel-controler"></a>
				<?php $icon_pause =  $patlan_icons["misc"]["icon-slider-pause"]; ?>
				<?php $icon_play =  $patlan_icons["misc"]["icon-slider-play"] ; ?>
				<a data-slide="pause" data-iconpause="<?php echo $icon_pause;?>" data-iconplay="<?php echo $icon_play;?>" href="#patlan-carousel-<?php echo (int) $parent_id;?>" class="pause <?php echo $icon_pause;?> carousel-controler"></a>
				<a data-slide="next" href="#patlan-carousel-<?php echo (int) $parent_id;?>" class="right  <?php echo $patlan_icons["misc"]["icon-slider-next"]; ?> carousel-controler"></a>
			</div>
			<?php endif; // if( $has_gallery ) ?>
			
		</div>
		
</div><!-- /thumbnail-wraper -->
</div> <!-- /post-thumbnail -->
