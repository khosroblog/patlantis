<?php global $post, $patlan_icons; ?>
	<article  lang="en" <?php post_class() ;?> >
		
		<?php if( has_post_thumbnail() || has_shortcode( get_the_content( $post->ID ), 'gallery' ) ): ?>
		<div class="post-thumbnail" >
			<div class="thumbnail-wraper " >
			
				<!-- Gallery Slider-<?php the_ID();?> -->
				<?php if( apply_filters("patlan_custom_slider", false) ): ?>
				<?php do_action("patlan_register_slider"); ?>
				<?php else: ?>
				<?php get_template_part("formats/gallery", "slider");?>
				<?php endif; ?>
				<!-- /Gallery Slider-<?php the_ID();?> -->
				
			</div>
		</div>
		<?php endif; ?>
		
		<div class="post-wrap" >
		
			<!-- Post Entry Meta -->
			<?php patlan_post_entrymeta(); ?>
			<!-- /Post Entry Meta -->
			
			<div class="post-content " >
				<h3 class="post-title" >
					<?php patlan_sticky_post_icon(); ?>
					<a href="<?php the_permalink();?>" alt="<?php the_title_attribute();?>" >
						<?php echo esc_html( get_the_title() );?>
					</a>
				</h3>
				
				<div class="article-content" >
					<?php echo patlan_post_content();?>
				</div>
				
				<?php echo patlan_post_categories() ;?>
				
			</div>
			
		</div>
	</article>