<?php 
	global $patlan_icons, $post;
		
	$icon_editpost_link = $patlan_icons["post-icons"]["icon-edit-post-link"];
	$format = ( get_post_format() )? get_post_format() : "standard" ; 
	$i = $patlan_icons["post-icons"]['icon-post-format'];
	$icon_post_format = ( in_array("format-{$format}", array_keys( $i ) ) )?  $i["format-".$format]: "" ;
	
?>
<div class="post-meta" >	
	<ul>
		<li class="table-row format-post" >
			<div class="table-cell" >
				<?php $dir = ( is_rtl() )? "right" : "left" ; ?>
				<i class="<?php echo $dir; ?>-arrow arrow-icon" ></i>
				<i  class="icon-post-format <?php echo esc_attr( $icon_post_format );?>" ></i>
			</div>
			
		</li>
		<li class="table-row post-date" >
			<a href="<?php the_permalink();?>" >
				<div class="table-cell column-1" >
					<span class="post-year" ><?php echo get_the_date("y");?></span>
					<span class="post-month" ><?php echo get_the_date("m");?></span>
				</div>
				<div class="table-cell column-2" >
					<span class="post-day" ><?php echo get_the_date("d");?></span>
				</div>
			</a>
		</li>
		<li class="table-row post-comments" >
			<div class="table-cell" >
				<span class="post-comments-count" >
				<?php comments_number( '0', '1', '%' ); ?>
					<i  class="icon-post-comment <?php echo $patlan_icons['post-icons']['icon-comments'];?>" ></i>
				</span>
			</div>
		</li>
		<?php edit_post_link( "<i class='{$icon_editpost_link}'></i>","<li class='table-row edit-post' ><div class='table-cell' >", "</div></li>" ); ?>
		<?php do_action("patlan_post_entrymeta_column"); ?>
		
	</ul>
</div>