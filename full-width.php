<?php 
/* Template Name: Full Width */
?>
<?php get_header(); global $post, $patlan_icons , $wp_query; ?>
	
	<!-- Main -->
	<div id="main-container" class="main row-fluid " >
	
	<!-- Main Section -->
	<?php 
		$without_sidebar = "span16";
		if( in_array( 'content-section-class', $_wp_theme_features ) ){
			$cs_class = $_wp_theme_features['content-section-class'];
			$without_sidebar = $cs_class[0]['without_sidebar'];
		}
		$without_sidebar = esc_attr( $without_sidebar );
	?>
	<section class="main-section  <?php echo $without_sidebar;?>" >
			<?php do_action("patlan_prepend_content_section"); ?>
			
			<!-- Content Section -->
			<section class="content-section" >
				<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
				
					<article  lang="en" <?php post_class() ;?> >
						
						<!-- Post Thumbnail -->
						<?php echo patlan_post_thumbnail(); ?>
						<!-- /Post Thumbnail -->
						
						<div class="post-wrap" >
						
							<div class="post-content " >
								<h3 class="post-title" ><?php patlan_sticky_post_icon(); ?><?php echo esc_html( get_the_title() ); ?></h3>

								<div class="article-content" >
									<?php echo patlan_post_content(); ?>
								</div>
								
								<?php $wp_link_pages = wp_link_pages(array(
										'before'   	=> '<div class="pagination pagination-small wp-link-paginate" ><h5>' . __('Pages:', 'patlantis') . '</h5><ul>',
										'after'    	=> '</ul></div>',
										'echo'		=> false
								)); ?> 
								<?php echo apply_filters("theme_wp_link_pages", $wp_link_pages ); ?>
								
							</div><!-- /Post Content -->
							
						</div><!-- /Post Wraper -->
					</article>
				<?php endwhile; else: ?>
					<article id="post-0" class="post no-results not-found">
						<div class="post-wrap">
							<div class="post-content">
								<h3 class="post-title">
								<?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'atlantis' ); ?>
								</h3>
								<div class="article-content">
								<?php get_search_form(); ?>
								</div>
							</div>
						</div>
					</article>
				<?php endif; // end post loop ?>
				
			</section>
			<!-- /Content Section -->
		
			<!-- Comments Section -->
			<section class="comments-section " >
				<div class="comments-wraper" >
					<ul class="commentlist">
						<?php comments_template(); ?>
					</ul>
				</div>
			</section>
			<!-- /Comments Section -->
		
	</section>
	<!-- /Content Section -->
	
		
	</div>
	<!-- /Main -->
	
<?php get_footer(); ?>