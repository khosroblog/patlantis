<?php get_header(); global $post, $patlan_icons ; ?>
	
	<!-- Main -->
	<div id="main-container" class="main row-fluid " >
	
		<!-- Left Sidebar -->
		<?php $sidebar_left = ( is_rtl() )? "right" : "left" ; ?>
		<?php get_sidebar("{$sidebar_left}"); ?>
		<!-- /Left Sidebar -->
		
		<!-- Main Section -->
		<?php 
			$content_section_class = patlan_content_section_class();
			$content_section_class = esc_attr( $content_section_class );
		?>
		<section class="main-section  <?php echo $content_section_class;?>" >
			<?php do_action("patlan_prepend_content_section"); ?>
			
			<!-- Content Section -->
			<section class="content-section" >
			<?php if( have_posts() ): while( have_posts() ): the_post(); ?>
			
				<article  lang="en" <?php post_class() ;?> >
					
					<!-- Post Thumbnail -->
					<?php echo patlan_post_thumbnail(); ?>
					<!-- /Post Thumbnail -->
					
					<div class="post-wrap" >
					
						<div class="post-content " >
							<h3 class="post-title" ><?php patlan_sticky_post_icon(); ?><?php echo esc_html( get_the_title() ); ?></h3>
							
							<div class="article-content" >
								<?php echo patlan_post_content();?>
							</div>
							
							<?php echo patlan_pages_paginate(); ?>
							
						</div><!-- /Post Content -->
					</div><!-- /Post Wrap -->
					
				</article>
				
			<?php endwhile; ?>
			<?php else: ?>
			<article id="post-0" class="post no-results not-found">
				<div class="post-wrap">
					<div class="post-content">
						<h3 class="post-title">
						<?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'pasargad' ); ?>
						</h3>
						<div class="article-content">
						<?php get_search_form(); ?>
						</div>
					</div>
				</div>
			</article>
			<?php endif; // end post loop ?>
			
			</section>
			<!-- /Content Section -->
			
			<!-- Comments Section -->
			<section class="comments-section " >
				<div class="comments-wraper" >
					<ul class="commentlist">
						<?php comments_template(); ?>
					</ul>
				</div>
			</section>
			<!-- /Comments Section -->
			
		</section>
		<!-- /Content Section -->
		
		<!-- Right Sidebar -->
		<?php $sidebar_right = ( is_rtl() )? "left" : "right" ; ?>
		<?php get_sidebar("{$sidebar_right}"); ?>
		<!-- /Right Sidebar -->
		
		
	</div>
	<!-- /Main -->
	
<?php get_footer(); ?>